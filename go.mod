module gitlab.com/conejo/prettifier

go 1.16

require (
	fyne.io/fyne/v2 v2.0.3
	github.com/go-xmlfmt/xmlfmt v0.0.0-20191208150333-d5b6f63a941b
)
