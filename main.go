package main

import (
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

func main() {

	appSize := fyne.NewSize(480, 320)

	a := app.New()
	w := a.NewWindow("Prettifier")

	w.Resize(appSize)

	toolbar := widget.NewToolbar(
		widget.NewToolbarAction(theme.ContentCutIcon(), func() {}),
		widget.NewToolbarAction(theme.ContentCopyIcon(), func() {}),
		widget.NewToolbarAction(theme.ContentPasteIcon(), func() {}),
		widget.NewToolbarSpacer(),
		widget.NewToolbarAction(theme.HelpIcon(), func() {
			log.Println("Display help")
		}),
	)

	dirtyAreaText := binding.NewString()

	dirtyArea := widget.NewMultiLineEntry()
	dirtyArea.Bind(dirtyAreaText)
	dirtyArea.PlaceHolder = "enter text to format here"

	status := binding.NewString()
	statusLabel := widget.NewLabelWithData(status)

	prettyButton := widget.NewButton("Prettify", func() {
		_ = status.Set("")

		r, err := pretty(dirtyArea.Text)
		if err != nil {
			_ = status.Set(err.Error())
			return
		}

		_ = dirtyAreaText.Set(r)
	})

	base64EncodeButton := widget.NewButton("b64 Encode", func() {
		_ = status.Set("")

		_ = dirtyAreaText.Set(Base64Encode(dirtyArea.Text))
	})

	base64DecodeButton := widget.NewButton("b64 Decode", func() {
		_ = status.Set("")

		v, err := Base64Decode(dirtyArea.Text)
		if err != nil {
			_ = status.Set("Could not BASE64 decode value")
			return
		}

		_ = dirtyAreaText.Set(v)
	})

	clearBtn := widget.NewButton("Clear", func() {
		_ = dirtyAreaText.Set("")
	})

	copyBtn := widget.NewButton("Copy", func() {
		w.Clipboard().SetContent(dirtyArea.Text)
	})

	widget.NewSeparator()

	buttons := container.NewHBox(prettyButton, widget.NewSeparator(), base64EncodeButton, base64DecodeButton, widget.NewSeparator(), clearBtn, copyBtn)

	topItems := container.NewVBox(toolbar, buttons)

	content := container.New(layout.NewBorderLayout(topItems, statusLabel, nil, nil), topItems, statusLabel, dirtyArea)

	w.SetContent(content)

	w.ShowAndRun()
}
