# prettifier

This project provides a desktop utility for:

- base64 encoding and decoding 
- formatting JSON and XML


## development

To build and run in one step:

```
go build; ./prettifier
```
