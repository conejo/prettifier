package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"github.com/go-xmlfmt/xmlfmt"
)

// FormatJSON prettifies unformatted valid json.
// If the supplied json is not valid, an error is returned.
func FormatJSON(ugly io.Reader) ([]byte, error) {
	var p interface{}
	err := json.NewDecoder(ugly).Decode(&p)
	if err != nil {
		return nil, err
	}

	return json.MarshalIndent(p, "", "  ")
}

// Base64Encode base64 encodes the supplied string
func Base64Encode(val string) string {
	return base64.StdEncoding.EncodeToString([]byte(val))
}

func padBase64String(val string) string {
	switch len(val) % 4 {
	case 1, 3:
		return fmt.Sprintf("%s=", val)
	case 2:
		return fmt.Sprintf("%s==", val)
	default:
		return val
	}
}

// Base64Decode base64 decodes the supplied string. If it can't, it returns an error.
func Base64Decode(val string) (string, error) {

	decodeVal := padBase64String(val)

	v, err := base64.StdEncoding.DecodeString(decodeVal)
	if err != nil {
		return "", err
	}

	return string(v), nil
}

func pretty(val string) (string, error) {
	val = strings.TrimSpace(val)

	if len(val) == 0 {
		return "", fmt.Errorf("%s", "empty val")
	}

	bytes := []byte(val)

	//check json
	if json.Valid(bytes) {
		pretty, err := FormatJSON(strings.NewReader(val))
		if err != nil {
			return "", err
		}

		return fmt.Sprintf("%s", pretty), nil
	}

	//check xml
	if bytes[0] == '<' {
		// this looks like the ticket for prettifying xml:
		// https://github.com/go-xmlfmt/xmlfmt/
		return xmlfmt.FormatXML(val, "", "  "), nil
	}

	//try base64 decoding
	v, err := Base64Decode(val)
	if err == nil {
		// it worked! lets recurse and prettify result
		return pretty(string(v))
	}

	return "", fmt.Errorf("%s", "could not process value")

}
