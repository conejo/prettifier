package main

import (
	"testing"
)

func TestBase64Encode(t *testing.T) {
	type args struct {
		val string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "empty val", args: args{val: ""}, want: ""},
		{name: "length 1", args: args{val: "1"}, want: "MQ=="},
		{name: "length 2", args: args{val: "12"}, want: "MTI="},
		{name: "length 3", args: args{val: "123"}, want: "MTIz"},
		{name: "length 4", args: args{val: "1234"}, want: "MTIzNA=="},
		{name: "length 5", args: args{val: "12345"}, want: "MTIzNDU="},
		{name: "length 6", args: args{val: "123456"}, want: "MTIzNDU2"},
		{name: "length 7", args: args{val: "1234567"}, want: "MTIzNDU2Nw=="},
		{name: "length 8", args: args{val: "12345678"}, want: "MTIzNDU2Nzg="},
		{name: "length 9", args: args{val: "123456789"}, want: "MTIzNDU2Nzg5"},
		{name: "length 10", args: args{val: "1234567890"}, want: "MTIzNDU2Nzg5MA=="},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Base64Encode(tt.args.val); got != tt.want {
				t.Errorf("Base64Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_padBase64String(t *testing.T) {
	type args struct {
		val string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "empty val", args: args{val: ""}, want: ""},
		{name: "length 2", args: args{val: "MQ"}, want: "MQ=="},
		{name: "length 3", args: args{val: "MTI"}, want: "MTI="},
		{name: "length 4", args: args{val: "MTIz"}, want: "MTIz"},
		{name: "length 6", args: args{val: "MTIzNA"}, want: "MTIzNA=="},
		{name: "length 7", args: args{val: "MTIzNDU"}, want: "MTIzNDU="},
		{name: "length 8", args: args{val: "MTIzNDU2"}, want: "MTIzNDU2"},
		{name: "length 10", args: args{val: "MTIzNDU2Nw"}, want: "MTIzNDU2Nw=="},
		{name: "length 11", args: args{val: "MTIzNDU2Nzg"}, want: "MTIzNDU2Nzg="},
		{name: "length 12", args: args{val: "MTIzNDU2Nzg5"}, want: "MTIzNDU2Nzg5"},
		{name: "length 14", args: args{val: "MTIzNDU2Nzg5MA"}, want: "MTIzNDU2Nzg5MA=="},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := padBase64String(tt.args.val); got != tt.want {
				t.Errorf("padBase64String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBase64Decode(t *testing.T) {
	type args struct {
		val string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{name: "empty", args: args{val: ""}, want: "", wantErr: false},
		{name: "1", args: args{val: "MQ=="}, want: "1", wantErr: false},
		{name: "1 no padding", args: args{val: "MQ"}, want: "1", wantErr: false},
		{name: "2", args: args{val: "MTI="}, want: "12", wantErr: false},
		{name: "2 no padding", args: args{val: "MTI"}, want: "12", wantErr: false},
		{name: "3", args: args{val: "MTIz"}, want: "123", wantErr: false},
		{name: "4", args: args{val: "MTIzNA=="}, want: "1234", wantErr: false},
		{name: "4 no padding", args: args{val: "MTIzNA"}, want: "1234", wantErr: false},
		{name: "5", args: args{val: "MTIzNDU="}, want: "12345", wantErr: false},
		{name: "5 no padding", args: args{val: "MTIzNDU"}, want: "12345", wantErr: false},
		{name: "6", args: args{val: "MTIzNDU2"}, want: "123456", wantErr: false},
		{name: "7", args: args{val: "MTIzNDU2Nw=="}, want: "1234567", wantErr: false},
		{name: "7 no padding", args: args{val: "MTIzNDU2Nw"}, want: "1234567", wantErr: false},
		{name: "8", args: args{val: "MTIzNDU2Nzg="}, want: "12345678", wantErr: false},
		{name: "8 no padding", args: args{val: "MTIzNDU2Nzg"}, want: "12345678", wantErr: false},
		{name: "9", args: args{val: "MTIzNDU2Nzg5"}, want: "123456789", wantErr: false},
		{name: "10", args: args{val: "MTIzNDU2Nzg5MA=="}, want: "1234567890", wantErr: false},
		{name: "10 no padding", args: args{val: "MTIzNDU2Nzg5MA"}, want: "1234567890", wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Base64Decode(tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("Base64Decode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Base64Decode() got = %v, want %v", got, tt.want)
			}
		})
	}
}
